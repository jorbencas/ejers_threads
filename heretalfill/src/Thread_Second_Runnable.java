
public class Thread_Second_Runnable extends Thread {

	public Thread_Second_Runnable() {
		// TODO Auto-generated constructor stub
	}


	//Mètode Run -----------------------------------
	public void run(){
		try {
			System.out.println("Thread sleeping for 5 secs");
			Thread.sleep(5000);
			System.out.println("Thread finishing");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//fi run
	//Metode main ------------------------------
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Thread_Second_runnable b=null;
		b=new Thread_Second_runnable(); //creacio fil
		
		Thread h = new Thread(b);
		//Comprovació estat abans cridar a start
		System.out.println("Abans cridar a start()");
		System.out.println("State:" + h.getState());
		System.out.println("Is Alive? = " + h.isAlive());
		System.out.println("\nInvocació a start()");
		h.start(); //inicia el fil
		//Comprovació estat despres cridar a start

		System.out.println("State:" + h.getState());
		System.out.println("Is Alive? = " + h.isAlive()+"\n");
		// invocacio a join i espera finalitzacio
		try{
			h.join();
		} catch (Exception ex){}
		//Comprovació estat despres finalització del fil
		System.out.println("\nDespres finalització del fil");
		System.out.println("State:" + h.getState());
		System.out.println("Is Alive? = " + h.isAlive());
	}

}
