
public class Thread_First_runnable implements Runnable {

	public Thread_First_runnable() {
		// TODO Auto-generated constructor stub

	}

	//Propietats --------------------------------
	private int c; //contador fil
	private String nomFil;
	//Constructor --------------------------------
	public Thread_First_runnable (String fil){
		this.nomFil=fil;
	}//fi constructor
	//Mètode Run -----------------------------------
	public void run(){
		c=0;
		while (c<=5){
			System.out.println ("Fil["+ this.nomFil+ "] C= " + c);
			c++;
		}
	}//fi run
	//Mètode main -------------------------------
	public static void main(String[] args){
		System.out.println("First Thread implementing interface Runnable \n");
		 int MAX_PRIORITY = 10;
		 int MIN_PRIORITY = 1;
		 int NORM_PRIORITY = 5;
		 
		Thread_First_runnable h=null;
		Thread t=null;
		for (int i=1;i<=3;i++){
			System.out.println("Fil ppal. Creant nou fil" + i);
			h=new Thread_First_runnable("fil"+(i+1));
			
			System.out.println("Fil ppal. Executant nou fil" + i);
			t = new Thread(h); //creacio fil
			if(i == 1) {
				t.setPriority(MAX_PRIORITY);
			}else if(i == 2){
				t.setPriority(MIN_PRIORITY);
			}else {
				t.setPriority(NORM_PRIORITY );
			}
			System.out.println(t.getPriority());
			t.start();
			//iniciar fil
		}
		System.out.println("3 fils creats...");
	} //fi main

}
