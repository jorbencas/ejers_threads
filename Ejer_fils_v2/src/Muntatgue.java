public class Muntatgue {
	private boolean[] valor;
	public int MAX_VECTOR = 5;
	public int nfabricats = 0;
	
	public int getNfabricats() {
		return nfabricats;
	}

	public void setNfabricats(int nfabricats) {
		this.nfabricats = nfabricats;
	}

	public Muntatgue() {
		this.valor = new boolean[MAX_VECTOR];
		inicialitzaCua();
	}


		public void inicialitzaCua() {
			for (int i=0; i < MAX_VECTOR; i++)
				this.valor[i] = false;
		}

	public synchronized void addCoche( int i) throws InterruptedException {
		
		while (this.valor[0]) {
			this.wait();
		}
		if (nfabricats==0) System.out.print("\nMUNTATGE COTXE " +i+ " :" + " \t\t\t [");
		mostarvector();
		System.out.println("]");
		
		this.setNfabricats(this.getNfabricats()+1);
		this.valor[0]=true;
		System.out.print("\nMUNTATGE COTXE " +i+ " :" + " \t\t\t [");
		mostarvector();
		System.out.println("]");
		Thread.sleep(250);
		this.notifyAll();
		
		
		while (this.valor[1] && this.getNfabricats() != 1) {
			wait();
		}
		this.setNfabricats(this.getNfabricats()+1);
		this.valor[1]=true;
		System.out.print("\nMUNTATGE COTXE " +i+ " :" + " \t\t\t [");
		mostarvector();
		System.out.println("]");
		Thread.sleep(250);
		this.notifyAll();
		
		while (this.valor[2] && this.getNfabricats()!=2) {
			this.wait();
		}
		this.setNfabricats(this.getNfabricats()+1);
		this.valor[2]=true;
		System.out.print("\nMUNTATGE COTXE " +i+ " :" + " \t\t\t [");
		mostarvector();
		System.out.println("]");
		Thread.sleep(250);
		this.notifyAll();
		
		while (this.valor[3]  && this.getNfabricats()!=3 ) {
			this.wait();
		}
		this.setNfabricats(this.getNfabricats()+1);
		this.valor[3]=true;
		System.out.print("\nMUNTATGE COTXE " +i+ " :" + " \t\t\t [");
		mostarvector();
		System.out.println("]");
		Thread.sleep(250);
		this.notifyAll();
		
		while (this.valor[4]  && this.getNfabricats()!=4) {
			this.wait();
		}
		this.setNfabricats(this.getNfabricats()+1);
		this.valor[4]=true;
		System.out.print("\nMUNTATGE COTXE " +i+ " : "+ " \t\t\t [");
		mostarvector();
		System.out.println("]");
		this.notifyAll();
		System.out.println("Coche montat");
		inicialitzaCua();
	}
	
	private void mostarvector() {
		for (int i=0; i < MAX_VECTOR; i++)
		System.out.print( this.valor[i] + "-");
	}
}
