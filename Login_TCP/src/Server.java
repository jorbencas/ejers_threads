import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void main(String[] args) throws InterruptedException {
		int puerto = 3001;
		ServerSocket server_socket;
		Socket socket;
		try {
			System.out.println("El servidor esta escuchando...");
			server_socket = new ServerSocket(puerto);
			
			
			for(int connections = 1; connections <= 2; connections++) {
				//server_socket.setSoTimeout(5000);
				socket = server_socket.accept();
				System.out.println(socket.getRemoteSocketAddress() + "Creado conexión " + connections);
				Thread server = new Thread(new Server_Thread(socket, connections));
				server.start();
			}
			
			System.out.println("Todos los mensages han sido enviados.");
			
			server_socket.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}