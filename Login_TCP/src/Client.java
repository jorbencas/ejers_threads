import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		int numport = 3001;
		Socket client;
		DataInputStream entrada;
		DataOutputStream salida;
		String mensaje = "";
		String mensa = "";
        Scanner teclado = new Scanner(System.in);
        boolean user = false;
        boolean passwd = false;
        boolean correcto = false;
		try {
			client = new Socket("localhost",numport);

			salida = new DataOutputStream(client.getOutputStream());
			entrada = new DataInputStream(client.getInputStream());

	        do {
	        	if (!user) {
	        		  System.out.println("Introduce el usuario: ");
				} else if (!passwd) {
					  System.out.println("Introduce el password: ");
				}
	          
	            mensaje = teclado.nextLine();
	           
	            salida.writeUTF(mensaje);

	            //Respuesta
	            mensa = entrada.readUTF();
				System.out.println(mensa);

				if(mensa.contains("laura")) {
					user = true;
				}else if(mensa.contains("l123")) {
					passwd = true;
				}
				
				if(user && passwd) {
					correcto = true;
				}
				
	        } while (!correcto);

			salida.close();
			entrada.close();
			client.close();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
    
}
