import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.sun.source.tree.DoWhileLoopTree;

public class Server_Thread implements Runnable{

	Socket socket;
	DataInputStream entrada;
	DataOutputStream salida;
	String mensa, mensaje;
	private int numclient;
	boolean user = false;
	boolean passwd = false;
	boolean correcto = false;
	String usuarioCorrecto;
	String contraseñaCorrecta;
	String dato;
	
	public Server_Thread(Socket server_socket, int numclient) {
		super();
		this.numclient = numclient;
		this.socket = server_socket;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			salida = new DataOutputStream(this.socket.getOutputStream());
			entrada = new DataInputStream(this.socket.getInputStream());
			
			do{
				dato = entrada.readUTF();
				if (!user) {
					System.out.println("Reciviendo usuario " + dato + " del cliente " + numclient);
					if (dato.trim().equalsIgnoreCase("laura")) {
						mensaje = "correcto";
						user = true;
					} else {
						mensaje = "incorrecto";
					}
					System.out.println("Enviando usuario " + dato +" del cliente " + numclient);
					this.usuarioCorrecto = dato;
					salida.writeUTF("el usuario " + this.usuarioCorrecto + " es " + mensaje);
				}else if (!passwd) {
					System.out.println("Reciviendo contrasenya " + dato + " del cliente " + numclient);
					if (dato.trim().equalsIgnoreCase("l123")) {
						mensaje = "correcto";
						passwd = true;
					} else {
						mensaje = "incorrecto";
					}
					System.out.println("Enviando contrasenya " + dato +" del cliente " + numclient);
					this.contraseñaCorrecta = dato;
					salida.writeUTF("La contrasenya " + this.contraseñaCorrecta + " es " + mensaje);
				}

				if(user && passwd) {
					correcto = true;
				}
				
			}while(!correcto);

				System.out.println("El usuario " + this.usuarioCorrecto + " se ha conectado con la contraseña " + this.contraseñaCorrecta + " con exito!");
				System.out.println("Cerrando servidor...");
				
				entrada.close();
				salida.close();
				socket.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


}
