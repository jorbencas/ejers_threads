import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cliente extends Thread  {

	private int port = 3000;
	byte[] buffer = new byte[1024];
	private String nombrecliente;
	int  i = 0;
	 String condicion;
	 
	 public Cliente(String nombrecliente) {
		super();
		this.nombrecliente = nombrecliente;
	}


	@Override
	    public void run() {
	        try {
	        	MulticastSocket socket = new MulticastSocket(this.port);
	            InetAddress direccionServidor = InetAddress.getByName("225.0.0.0");
	            socket.joinGroup(direccionServidor);
	            //Thread.sleep(2000);
	            System.out.println( this.nombrecliente + " suscribiendo se a la dirección IP " + direccionServidor.getHostAddress() + ":" + this.port);
	           
	            do {
	            	buffer = new byte[1024];
	            	DatagramPacket msgPacket = new DatagramPacket(buffer, buffer.length);
	            	socket.receive(msgPacket);
	            	
	                String msg = new String(msgPacket.getData());
	                System.out.println( "\t" + this.nombrecliente + " hemos recibido " + msg);
	                condicion = msg.split(" ")[0];
	                System.out.println(" ");
	                Thread.sleep(1000);
	            	i = i + 10;

	            }while(!condicion.equalsIgnoreCase(100 + ""));
	           
	            socket.close();
	            //socket.disconnect();
	        } catch (SocketException ex) {
	           
	        } catch (UnknownHostException ex) {
	            
	        } catch (IOException ex) {
	            
	        } catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	    }
	 
	 
}
