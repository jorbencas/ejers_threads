import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main(String[] args) throws IOException {
		int  puerto = 3001;
		 ServerSocket server_socket;
		 Socket socket;
		 try {
			 System.out.println("El servidor esta escuchando...");
				server_socket = new ServerSocket(puerto);
				for(int i = 0; i < 3; i++){
					socket = server_socket.accept();
					Thread client = new Thread(new ClientThread(socket, i));
					client.start();
					client.join();
				};
				System.out.println("Todos los mensajes han sido enviados.");
				server_socket.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
