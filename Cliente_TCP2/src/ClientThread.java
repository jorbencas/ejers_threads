import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientThread implements Runnable {

	private int numclient;
	
	 Socket socket;
	 DataInputStream entrada;
	 DataOutputStream salida;
	 String mensa = "11";
	 
	public ClientThread(Socket socket, int numclient) {
		super();
		this.numclient = numclient;
		this.socket = socket;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			
			salida = new DataOutputStream(socket.getOutputStream());
			entrada = new DataInputStream(socket.getInputStream());

			String mensage = "Hola cliente " + numclient;
			salida.writeUTF(mensage);
			
			entrada.close();
			salida.close();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
