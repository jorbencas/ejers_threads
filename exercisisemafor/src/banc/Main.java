/**
 * 
 */
package banc;

/**
 * @author jorge
 *
 */
public class Main {

	/**
	 * 
	 */
	public Main() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Caixerbanc caixer1 = new Caixerbanc("fil1");
		Thread accio1 = new Thread(caixer1); 
		
		Caixerbanc caixer2 = new Caixerbanc("fil2");
		Thread accio2 = new Thread(caixer2); 
		
		Caixerbanc caixer3 = new Caixerbanc("fil3");
		Thread accio3 = new Thread(caixer3); 
		
		accio1.start();
		accio2.start();
		accio3.start();
		
		accio1.join();
		accio2.join();
		accio3.join();
		
		System.out.println("Saldu Final: 1030");
		
	}

}
