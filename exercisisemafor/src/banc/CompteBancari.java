package banc;

public class CompteBancari extends Thread {

	private float Saldo = 0;
	private Semafor semafor = new Semafor(1);

	public float getSaldo() {
		return Saldo;
	}

	public void setSaldo(float saldo) {
		Saldo = saldo;
	}

	public CompteBancari() {
		// TODO Auto-generated constructor stub
	}
	
	public CompteBancari(float saldo) {
		// TODO Auto-generated constructor stub
		this.setSaldo(saldo);
	}
	

	public void ingressar(float diners) { 
		semafor.sendwait();
		float saldo;
		
		float aux;

		aux=getSaldo();
		aux=aux+diners;
		System.out.println("El saldo actual es de  " + aux );
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		saldo=aux;
		
		setSaldo(saldo);
		semafor.sendsignal();

	}


	public void treure(float diners) {
		semafor.sendwait();
		float saldo;
		float aux;

		aux=getSaldo();
		aux= aux - diners;
		System.out.println("El saldo actual es de " + diners );
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		saldo=aux;
		
		setSaldo(saldo);
		semafor.sendsignal();
	}
	
}
