package banc;

public class Caixerbanc extends Thread {
	
	@Override
	public void run(){
		CompteBancari conter = new CompteBancari();
		if(this.getName().equals("fil1")) {
			System.out.println( "En el caixer: " + this.getName() + " se va ha ingresar: 1000 ");
			conter.ingressar(1000);
			System.out.println( "En el caixer: " + this.getName() + " se va ha traure: 500 ");
			conter.treure(500);
			System.out.println("En el caixer: " + this.getName() + " saldo Final: " + conter.getSaldo());
		}else {
			if(this.getName().equals("fil2")) {
				System.out.println("En el caixer: " + this.getName() + " se va ha ingresar: 200");
				conter.ingressar(200);
				System.out.println( "En el caixer: "  + this.getName() + " se va ha traure: 50");	
				conter.treure(50);
				System.out.println("En el caixer: " + this.getName() + " saldo Final: " + conter.getSaldo());
			}else{
				System.out.println("En el caixer: " + this.getName() + " se va ha ingresar: 400");
				conter.ingressar(400);
				System.out.println( "En el caixer: " + this.getName() + " se va ha traure: 20");
				conter.treure(20);
				System.out.println("En el caixer: " + this.getName() + " saldo Final: " + conter.getSaldo());
			}
		}

	}

	//************************************
	public Caixerbanc(String nom) {
		this.setName(nom);
	}

}
