import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class InterlocutorA implements Runnable {

	private String message;
	private Red red = new Red();
	private int port = 5556;
	InetAddress adrecaDesti;
	
	public InterlocutorA(String message) {
		super();
		this.message = message;
	}

	@Override
	public void run() {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
				try {
					 adrecaDesti = InetAddress.getByName("localhost");
				} catch (UnknownHostException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
		byte[] res = this.message.getBytes();
		DatagramPacket pac = new DatagramPacket(res, res.length, adrecaDesti,5555);
		try {
			socket.send(pac);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			this.red.enviarmesague("A", pac);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		socket.close();
		
		DatagramSocket socket2 = null;
		try {
			socket2 = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
				try {
					socket2.receive(pac);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
		
		
		
		try {
			this.red.recibirmesague("A", this.port, pac);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		socket2.close();
	}

}
