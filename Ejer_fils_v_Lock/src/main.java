
public class main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Muntatgue muntague = new Muntatgue();
		Coche c1 = new Coche("Motor",2000, muntague);
		Coche c2 = new Coche("Sistema Electric",2000, muntague);
		Coche c3 = new Coche("Acessoris",2000, muntague);
		Coche c4 = new Coche("vidres",2000, muntague);
		Coche c5 = new Coche("Rodes",2000, muntague);
		
		c1.start();
		c2.start();
		c3.start();
		c4.start();
		c5.start();
		
		c1.join();
		c2.join();
		c3.join();
		c4.join();
		c5.join();
		System.out.println("Fin de cadena de montague, todos los coches se han montado!!!");
	}

} 
