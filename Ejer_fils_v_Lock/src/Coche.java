
public class Coche extends Thread {
	private String nomfil = "";
	private long time = 0;
	private Muntatgue muntague;

	public Coche(String nomfil, int time, Muntatgue muntague) {
		super();
		this.nomfil = nomfil;
		this.time = time;
		this.muntague = muntague;
	}
	

	public String getNomfil() {
		return nomfil;
	}

	public void setNomfil(String nomfil) {
		this.nomfil = nomfil;
	}
	
	
	
	public long getTime() {
		return time;
	}


	public void setTime(long time) {
		this.time = time;
	}


	public void run() {
		for(int i = 0; i < 30; i++) {
			try {
				muntague.addCoche(i);
				this.sleep(this.getTime());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
