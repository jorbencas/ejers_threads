import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Muntatgue {
	private boolean[] valor;
	public int MAX_VECTOR = 5;
	public int nfabricats = 0;
	private Lock lock;
	private Condition motor;
	private Condition sistema_electric;
	private Condition acesoris;
	private Condition vidres;
	private Condition rodes;
	
	public int getNfabricats() {
		return nfabricats;
	}

	public void setNfabricats(int nfabricats) {
		this.nfabricats = nfabricats;
	}

	public Muntatgue() {
		this.valor = new boolean[MAX_VECTOR];
		this.lock = new ReentrantLock();
		this.motor = lock.newCondition();
		this.sistema_electric = lock.newCondition();
		this.acesoris = lock.newCondition();
		this.vidres = lock.newCondition();
		this.rodes = lock.newCondition();
		inicialitzaCua();
	}


		public void inicialitzaCua() {
			for (int i=0; i < MAX_VECTOR; i++)
				this.valor[i] = false;
		}

	public void addCoche( int i) throws InterruptedException {
		
		this.lock.lock();
		if(this.lock.tryLock()) {
			try {

				while (this.valor[0]) {
					this.motor.await();
				}
				
				if (nfabricats==0) System.out.print("\nMUNTATGE COTXE " +i+ " : "+ " \t [" + mostarvector() + "]");
				
				this.setNfabricats(this.getNfabricats()+1);
				this.valor[0]=true;
				
				System.out.print("\nMUNTATGE COTXE " +i+ " : "+ " \t [" + mostarvector() + "]");
				
				Thread.sleep(250);
				this.sistema_electric.signal();;
				
				} finally {
					this.lock.unlock();
				}
			
		}else {
			System.out.println("Error e ");
		}
		
		
		this.lock.lock();
		if(this.lock.tryLock()) {
			try {

				while (this.valor[1] && this.getNfabricats() != 1) {
					this.sistema_electric.await();
				}
				this.setNfabricats(this.getNfabricats()+1);
				this.valor[1]=true;
				System.out.print("\nMUNTATGE COTXE " +i+ " : "+ " \t [" + mostarvector() + "]");
				Thread.sleep(250);
				this.acesoris.signal();
				
				} finally {
					this.lock.unlock();
				}
		}else {
			System.out.println("Error e ");
		}
		
		
		
		this.lock.lock();
		if(this.lock.tryLock()) {
			try {

				while (this.valor[2] && this.getNfabricats()!=2) {
					this.acesoris.await();
				}
				this.setNfabricats(this.getNfabricats()+1);
				this.valor[2]=true;
				System.out.print("\nMUNTATGE COTXE " +i+ " : "+ " \t [" + mostarvector() + "]");
				Thread.sleep(250);
				this.vidres.signal();
				
				} finally {
					this.lock.unlock();
				}
		}else {
			System.out.println("Error e ");
		}
		this.lock.lock();
		if(this.lock.tryLock()) {
			try {
				while (this.valor[3]  && this.getNfabricats()!=3 ) {
					this.vidres.await();;
				}
				this.setNfabricats(this.getNfabricats()+1);
				this.valor[3]=true;
				System.out.print("\nMUNTATGE COTXE " +i+ " : "+ " \t [" + mostarvector() + "]");
				Thread.sleep(250);
				this.rodes.signal();
				
				} finally {
					this.lock.unlock();
				}
		}else {
			System.out.println("Error e ");
		}
		this.lock.lock();
		if(this.lock.tryLock()) {
			try {

				while (this.valor[4]  && this.getNfabricats()!=4) {
					this.rodes.await();
				}
				this.setNfabricats(this.getNfabricats()+1);
				this.valor[4]=true;
				System.out.print("\nMUNTATGE COTXE " +i+ " : "+ " \t [" + mostarvector() + "]\n");
				System.out.println("Coche montat");
				inicialitzaCua();
				this.motor.signal();
				} finally {
					this.lock.unlock();
				}
		}else {
			System.out.println("Error error ");
		}
		
		
		
		
		
		
	}
	
	private String mostarvector() {
		String cad = "";
		for (int i=0; i < MAX_VECTOR; i++) {
			if(0 == i)
				 cad +=  "Motor: " + this.valor[i] + ", ";
			 else if(1 == i )
				 cad +=  "Sistema Electric: " + this.valor[i] + ", ";
			 else if(2 == i)
				 cad +=  "Acessoris: " + this.valor[i] + ", ";
			 else if(3 == i)
				 cad +=  "Vidres: " + this.valor[i] + ", ";	 
			 else if(4 == i)
				 cad +=  "Rodes: " + this.valor[i];
		}
		return cad;
	}
}
