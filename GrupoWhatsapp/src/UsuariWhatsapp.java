
public class UsuariWhatsapp extends Thread {
	private String nom = "";
	private XatWhatsapp xat = null;
	private boolean situacion = false;
	public UsuariWhatsapp(String nom, XatWhatsapp xat) {
		super();
		this.nom = nom;
		this.xat = xat;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			//this.xat.inicialitzarmatriu();
			this.xat.holaadeu(situacion, this);
			this.xat.pregunta(this);
			this.sleep(2000);
			this.xat.resposta(this);
			this.setSituacion(true);
			this.xat.holaadeu(situacion, this);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isSituacion() {
		return situacion;
	}

	public void setSituacion(boolean situacion) {
		this.situacion = situacion;
	}
	
	
	
	
}
