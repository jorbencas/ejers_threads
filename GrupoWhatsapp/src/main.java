
public class main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		XatWhatsapp xat = new XatWhatsapp();
		//xat.inicialitzarmatriu();
		UsuariWhatsapp usuari1 = new UsuariWhatsapp("Andreu", xat);
		UsuariWhatsapp usuari2 = new UsuariWhatsapp("Beatriz", xat);
		UsuariWhatsapp usuari3 = new UsuariWhatsapp("Carlos", xat);
		UsuariWhatsapp usuari4 = new UsuariWhatsapp("Delia", xat);
		UsuariWhatsapp usuari5 = new UsuariWhatsapp("Elena", xat);
		UsuariWhatsapp usuari6 = new UsuariWhatsapp("Hector", xat);
		UsuariWhatsapp usuari7 = new UsuariWhatsapp("Inés", xat);
		UsuariWhatsapp usuari8 = new UsuariWhatsapp("Jorge", xat);
		UsuariWhatsapp usuari9 = new UsuariWhatsapp("Marcos", xat);
		UsuariWhatsapp usuari10 = new UsuariWhatsapp("Llorenç", xat);

		usuari1.start();
		usuari2.start();
		usuari3.start();
		usuari4.start();
		usuari5.start();
		usuari6.start();
		usuari7.start();
		usuari8.start();
		usuari9.start();
		usuari10.start();
		
	 	usuari1.join();
	 	usuari2.join();
	 	usuari3.join();
	 	usuari4.join();
	 	usuari5.join();
	 	usuari6.join();
	 	usuari7.join();
	 	usuari8.join();
	 	usuari9.join();
	 	usuari10.join();
	}

}
