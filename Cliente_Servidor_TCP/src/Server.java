import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

	public static void main(String[] args) throws IOException {
		int  puerto = 3001;
		ServerSocket server_socket;
		Socket socket;
		DataInputStream entrada;
		DataOutputStream salida;
		String mensa = "11";
		boolean iterar = true;
		try {
			System.out.println("Esperant conexió...");
			server_socket = new ServerSocket(puerto);
			socket = server_socket.accept();
			System.out.println("OK, conectat.");
			salida = new DataOutputStream(socket.getOutputStream());
			entrada = new DataInputStream(socket.getInputStream());

			while(iterar) {

				mensa = entrada.readUTF();

				if(mensa.contains("adeu")) {
					System.out.println("\t\t\t[Cliente] Fins ara");
					iterar = false;
				}else{

					System.out.println("\t\t\t[Cliente] " + mensa);

					System.out.println("Escribe tu mensague: ");
					Scanner teclado = new Scanner(System.in);
					String texto = teclado.nextLine();
					if(texto.contains("adeu")) {
						iterar = false;
					}
					salida.writeUTF(texto);
					if (!texto.contains("adeu")){
						if(!mensa.contains("adeu")) {
							iterar = true;
						}
					}

				}
			}


			System.out.println("Finalitzant conexio");

			entrada.close();
			salida.close();
			System.out.println("Ok conexio tancada");
			socket.close();
			server_socket.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
