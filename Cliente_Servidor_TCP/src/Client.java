import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	public static void main(String args[]) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub

		int numport = 3001;
		Socket client;
		DataInputStream entrada;
		DataOutputStream salida;
		String mensague = "";
		String mensa = "";
		boolean iterar = true;

		try {
			System.out.println("Conectant al servidor...");
			client = new Socket(String.valueOf(args[0]),numport);
			System.out.println("OK, conectat.");	
			salida = new DataOutputStream(client.getOutputStream());
			entrada = new DataInputStream(client.getInputStream());

			while(iterar) {
				if(mensa.contains("adeu")) {
					System.out.println("\t\t\t[Servidor] Fins ara");
					iterar = false;
				}else{
					System.out.println("Escribe tu mensague: ");
					Scanner teclado = new Scanner(System.in);
					String texto = teclado.nextLine();
					if(texto.contains("adeu")) {
						iterar = false;
					}
					salida.writeUTF(texto);
					if(!texto.contains("adeu")) {
						
						mensa = entrada.readUTF();
						if(!mensa.contains("adeu")){
							System.out.println("\t\t\t[Servidor] " + mensa);
							iterar = true;
						}

					}	


				}
			}

			System.out.println("Ok conexio tancada");

			salida.close();
			entrada.close();
			System.out.println("Finalitzant conexio");
			client.close();

		} catch (Exception e) {
			// TODO: handle exception
		}


	}

}
