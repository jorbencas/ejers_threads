import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class main {

	public static void main(String[] args) throws UnknownHostException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduix un nom e domini: ");
		String nombre = sc.nextLine();
		InetAddress addr = InetAddress.getByName(nombre);
		
		System.out.println("Nom: " + addr.getHostName());
		System.out.println("Nom canonic: " + addr.getCanonicalHostName());
		System.out.println("Adreça Local: " + addr.isMCGlobal());
		System.out.println("Multicast" + addr.isMulticastAddress());

		System.out.println("Introduix primer bit");
		int num1 = sc.nextInt();
		
		System.out.println("Introduix el segun bit");
		int num2 = sc.nextInt();
		
		System.out.println("Introdueix el tercer bit");
		int num3 = sc.nextInt();
		System.out.println("Intoduix el cuart bit");
		int num4 = sc.nextInt();
		
		byte[] ipAddr = new byte[]{(byte) num1, (byte) num2, (byte) num3, (byte) num4};
		 addr = InetAddress.getByAddress(ipAddr);
		System.out.println("Nom:" + addr.toString());
		System.out.println("Nom canonic: " + addr.getCanonicalHostName());
		System.out.println("Adreça Local: " + addr.isMCGlobal());
		System.out.println("Multicast" + addr.isMulticastAddress());
	}

}
