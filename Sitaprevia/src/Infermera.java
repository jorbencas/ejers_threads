
public class Infermera extends Thread {

	private String[] consulta = new String[8];
	private int hor = 10;
	private int min = 0;
	private int cont = 0;

	public void inicialitzarconsulta() {
		for(int  i = 0; i <= 7; i ++) {
			this.consulta[i] = " "; 
		}
	}


	public synchronized void atendrecita(Pacient p) throws InterruptedException {
		System.out.println("pacient " + p.getNum() + " solicitant la cita");
		this.sleep(1000);
		if(cont < this.consulta.length) {
			p.setHoraE(hor);
			p.setHoraS(min);
			this.consulta[cont] = "" + hor + ":" + min + "";
			System.out.println("\t pacient " + p.getNum() + " rebrera cita previa per a les " + p.getHoraE() + ":" + p.getHoraS());

			if(min < 45) {
				min = min + 15;
			}else if(min == 45 ){
				hor = hor + 1;
				min = 0;
			}
			
		} else {
			System.out.println("\t pacient " + p.getNum() +  " no hi han consultes disponibles");
		}
		cont++;
	}
	
}