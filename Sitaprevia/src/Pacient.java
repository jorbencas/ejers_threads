
public class Pacient extends Thread {
	private int num= 0;
	private Infermera infermera = null;
	private int horaE = 0;
	private int horaS = 0;
	
	
	public Pacient(int num, Infermera infermera) {
		super();
		this.num = num;
		this.infermera = infermera;
	}

	public void run() {
		System.out.println("\n pacient " + this.num + " entrant en el ambulatori ... \n");
		this.infermera.inicialitzarconsulta();
		try {
			this.sleep(1000);
			this.infermera.atendrecita(this);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\n pacient " + this.num + " eixint del ambulatori \n");
		try {
			this.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getHoraE() {
		return horaE;
	}



	public void setHoraE(int horaE) {
		this.horaE = horaE;
	}



	public int getHoraS() {
		return horaS;
	}



	public void setHoraS(int horaS) {
		this.horaS = horaS;
	}
	
	
}
