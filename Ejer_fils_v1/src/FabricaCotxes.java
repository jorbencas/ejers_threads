


public class FabricaCotxes {

	//================================================================
	//Métode main
	public static void main(String[] args) throws InterruptedException{
		 
		System.out.println("Inici de Fil principal de  Runnable_Cotxes2");
		
		FilPeca a = new FilPeca();
		
		Motor m1 = new Motor(a);
		Sistema_Electric s1 = new Sistema_Electric(a);
		Acessoris a1 = new Acessoris(a);
		Vidres v1 = new Vidres(a);
		Rodes r1 = new Rodes(a);
		
		s1.start();	//iniciar fil Sistem Electric
		m1.start();	//iniciar fil Motor
		a1.start();	//iniciar fil Acessoris
		
		v1.start();	//iniciar fil Vidres
		
		
		r1.start();	//iniciar fil Rodes
		
		s1.join(); //espera finalització Sistem Electric
		m1.join(); //espera finalització Motor	
		a1.join(); //espera finalització Acessoris
		
		v1.join(); //espera finalització Vidres
		
		
		r1.join(); //espera finalització Rodes
		System.out.println("\nFi de Fil principal de  Runnable_Cotxes2");
		
	} //fi main

}