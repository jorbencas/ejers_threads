import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientThread implements Runnable {

	private int numclient;
	
	 Socket socket;
	 DataInputStream entrada;
	 DataOutputStream salida;
	 String mensa = "11";
	 private ServerSocket server_socket;
	public ClientThread(ServerSocket server_socket, int numclient) {
		super();
		this.numclient = numclient;
		this.server_socket = server_socket;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			socket = this.server_socket.accept();
			salida = new DataOutputStream(socket.getOutputStream());
			entrada = new DataInputStream(socket.getInputStream());

			String mensage = "Hola Cliente " + numclient;
			salida.writeUTF(mensage);
			
			entrada.close();
			salida.close();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
