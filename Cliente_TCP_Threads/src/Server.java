import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main(String[] args) throws IOException {
		int  puerto = 3001;
		 ServerSocket server_socket;
		
		 try {
			 System.out.println("El servidor esta escuchando...");
				server_socket = new ServerSocket(puerto);

				for(int  i = 0; i < 3; i++){
					Thread client = new Thread(new ClientThread(server_socket, i));
					client.start();
					client.join();
					Thread.sleep(1000);
				};
				System.out.println("Todos los mensagues han sido enviados.");
				server_socket.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
