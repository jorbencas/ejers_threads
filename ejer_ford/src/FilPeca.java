import java.util.ArrayList;

//FIL EXECUCIO
//======================================================================
public class FilPeca{
	private ArrayList<String> listacoches = new ArrayList<String>(5);
	public ArrayList<String> getListacoches() {
		return listacoches;
	}

	public void setListacoches(ArrayList<String> listacoches) {
		this.listacoches = listacoches;
	}

	public boolean isMotor() {
		return motor;
	}

	public void setMotor(boolean motor) {
		this.motor = motor;
	}

	public boolean isSistema_electric() {
		return sistema_electric;
	}

	public void setSistema_electric(boolean sistema_electric) {
		this.sistema_electric = sistema_electric;
	}

	public boolean isAcessoris() {
		return acessoris;
	}

	public void setAcessoris(boolean acessoris) {
		this.acessoris = acessoris;
	}

	public boolean isVidres() {
		return vidres;
	}

	public void setVidres(boolean vidres) {
		this.vidres = vidres;
	}

	public boolean isRodes() {
		return rodes;
	}

	public void setRodes(boolean rodes) {
		this.rodes = rodes;
	}

	private int cont = 1;
	private int numcoche = 0;
	private boolean motor = false;
	private boolean sistema_electric = false;
	private boolean acessoris = false;
	private boolean vidres = false;
	private boolean rodes = false;

	public FilPeca() {
	}	

	public synchronized void addMotor() throws InterruptedException {
		if(this.listacoches.size() == 0) {
			this.listacoches.add("Motor");
			this.setMotor(true);
			System.out.println(this.toString());
			this.notifyAll();
		}else {
			this.wait();
		}
	}

	public synchronized void addSistem_Electric() throws InterruptedException {
		if(motor == true) {
			this.listacoches.add("Sistema Electric");
			this.setSistema_electric(true);
			this.setMotor(false);
			System.out.println(this.toString());
			this.notifyAll();
		}else {
			this.wait();
		}
	}

	public synchronized void addAcessorio() throws InterruptedException {
		if(sistema_electric  == true ) {
			this.listacoches.add("Acessoris");
			this.setAcessoris(true);
			this.setSistema_electric(false);
			System.out.println(this.toString());
			this.notifyAll();
		}else {
			this.wait();
		}
	}
	public synchronized void addVidres() throws InterruptedException {
		if(acessoris  == true) {
			this.listacoches.add("vidres");
			this.setVidres(true);
			this.setAcessoris(false);
			System.out.println(this.toString());
			this.notifyAll();
		}else {
			this.wait();
		}
	}

	public synchronized void addRodes() throws InterruptedException {
		if(vidres  == true ) {
			this.listacoches.add("Rodes");
			this.setRodes(true);
			this.setVidres(false);
			System.out.println(this.toString());
			if(this.listacoches.size() == 5) {
				Noucoche();
			} 

			this.notifyAll();
		}else {
			this.wait();
		}
	}

	private void Noucoche() {
		System.out.println( "El coche " + cont + " se ha generado." );
		cont ++;

		this.numcoche = 0;
		this.listacoches = new ArrayList<String>();
		this.setMotor(false);
		this.setSistema_electric(false);
		this.setAcessoris(false);
		this.setVidres(false);
		this.setRodes(false);
	}
	@Override
	public String toString() {
		return  "Coche [" +  this.getListacoches() + "]";
	}


}