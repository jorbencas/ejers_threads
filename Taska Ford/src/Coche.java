import java.util.ArrayList;

public class Coche {
	ArrayList<Piezas> listaPiezas = new ArrayList<>();

    public Coche() {
    }

    public ArrayList<Piezas> getListaPiezas() {
        return listaPiezas;
    }

    public void setListaPiezas(ArrayList<Piezas> listaPiezas) {
        this.listaPiezas = listaPiezas;
    }

    @Override
    public String toString() {
        return "Coche{" + "listaPiezas=" +  + '}';
    }
    
    public String leerpiezas() {
    	String cad = "";
    	for(int i = 0; i < this.getListaPiezas().size(); i++) {
    		cad = this.getListaPiezas().get(i).toString();
    	}
		return cad;
    }
}
