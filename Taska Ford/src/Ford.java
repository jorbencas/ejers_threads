import java.util.ArrayList;

public class Ford {


	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		ArrayList<Piezas> piezasList = AñadirPiezas();
        ArrayList<Piezas> piezasList2 = AñadirPiezas();
        ArrayList<Piezas> piezasList3 = AñadirPiezas();

        Proveedor prov1 = new Proveedor("Mercedes", piezasList, new Coche());
        
        Proveedor prov2 = new Proveedor("BMV", piezasList2, new Coche());
        
        Proveedor prov3 = new Proveedor("Peugueot", piezasList3, new Coche());
        
        		
        prov1.start();
        prov2.start();
        prov3.start();
        
        prov1.join();
        prov2.join();
        prov3.join();
		
	}
	
	static ArrayList<Piezas> AñadirPiezas () throws InterruptedException{
		 //String[] piezasTipo = new String[5];
		 ArrayList<String> piezasTippo = new ArrayList<String>(); 
		 String pieza = "";
		 piezasTippo.add("Motor");
		 piezasTippo.add("Vidres");
		 piezasTippo.add("Rodes");
		 piezasTippo.add("Sistema Electrico");
		 piezasTippo.add("Accesoris");
		 
	        ArrayList<Piezas> listaPiezasEjemplo = new ArrayList<>();
	        for (int i = 0; i < 29; i++) {
	        	for (int j = 0; j < piezasTippo.size(); j++) {
	        		listaPiezasEjemplo.add(new Piezas(piezasTippo.get(j).toString(), i));
	        	}
	        }
	        return listaPiezasEjemplo;
	}
}
