
public class Piezas extends Thread {

	private int id; //contador fil
	private String nom = "";
	public Piezas(String string, int id) {
		// TODO Auto-generated constructor stub
		this.nom = string;
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@Override
	public String toString() {
		return  nom  + "[" + id + "]";
	}
	
	
	

}
