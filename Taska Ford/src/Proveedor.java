import java.util.ArrayList;

public class Proveedor extends Thread {

	private String nombreProveedor;
	private ArrayList<Piezas> listaPiezas;
	private Coche coche;

	public Proveedor(String nombreProveedor, ArrayList<Piezas> listaPiezas, Coche coche) {
		this.nombreProveedor = nombreProveedor;
		this.listaPiezas = listaPiezas;
		this.coche = coche;
	}

	@Override
	public void run() {

		for(int i = 0; i < 29; i++ ) {
			this.añadirPiezaAVehiculo(i);
			try {
				this.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public synchronized void añadirPiezaAVehiculo(int i) {
		this.setName(nombreProveedor);
		for(int j = 0; j < this.getListaPiezas().size();) {
			if(j == 4) {
				System.out.println("Soy el thread " + this.getName() + " y voy a introducir la pieza " + this.getPiezaAleatoria(j).getNom() + this.getPiezaAleatoria(j).getId());
				this.coche.getListaPiezas().add(this.getPiezaAleatoria(j));
				this.notifyAll();
				System.out.println("Ahora el vehiculo tiene " + coche.getListaPiezas().size() + " piezas");
				j++;
				try {
					this.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				j = 0;
			}
		}
		
		

	}

	public synchronized Piezas getPiezaAleatoria(int i) {
		return this.listaPiezas.get(i);
	}



	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public ArrayList<Piezas> getListaPiezas() {
		return listaPiezas;
	}

	public void setListaPiezas(ArrayList<Piezas> listaPiezas) {
		this.listaPiezas = listaPiezas;
	}

	public Coche getCochazo() {
		return coche;
	}

	public void setCochazo(Coche cochazo) {
		this.coche = cochazo;
	}

	@Override
	public String toString() {
		return "Proveedor{" + "nombreProveedor=" + nombreProveedor + ", listaPiezas=" + listaPiezas + ", coche=" + coche + '}';
	}
	public Proveedor() {
		// TODO Auto-generated constructor stub
	}

}
