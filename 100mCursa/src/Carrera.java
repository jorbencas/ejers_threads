import java.util.ArrayList;

public class Carrera extends Thread {

	private boolean tiempodesalida = false;
	private ArrayList<Atleta> corredores = new ArrayList<Atleta>();
	
	
	public ArrayList<Atleta> getCorredores() {
		return corredores;
	}

	public void setCorredores(ArrayList<Atleta> corredores) {
		this.corredores = corredores;
	}

	public boolean iniciarCarrera() throws InterruptedException {
        System.out.print("Preparados ");
        Thread.sleep(1000);
        System.out.print("Listos ");
        Thread.sleep(1000);
        System.out.print("Ya!");
        this.setTiempodesalida(true);
        System.out.println(" ");
        return this.tiempodesalida;
	}
	
	public void mostrarResultado() {
        for (int i = 0; i < this.corredores.size(); i++) {
            System.out.println(this.corredores.get(i).toString());
        }
	}

	public synchronized boolean isTiempodesalida() {
		return tiempodesalida;
	}

	public void setTiempodesalida(boolean tiempodesalida) {
		this.tiempodesalida = tiempodesalida;
	}
	
}
