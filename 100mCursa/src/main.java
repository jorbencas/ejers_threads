
public class main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Carrera carrera = new Carrera();
		Atleta atleta1 = new Atleta(1, carrera);
		Atleta atleta2 = new Atleta(2, carrera);
		Atleta atleta3 = new Atleta(3, carrera);
		Atleta atleta4 = new Atleta(4, carrera);
		Atleta atleta5 = new Atleta(5, carrera);
		Atleta atleta6 = new Atleta(6, carrera);
		Atleta atleta7 = new Atleta(7, carrera);
		Atleta atleta8 = new Atleta(8, carrera);
		
		atleta1.start();
		atleta2.start();
		atleta3.start();
		atleta4.start();
		atleta5.start();
		atleta6.start();
		atleta7.start();
		atleta8.start();
		
		atleta1.join();
		atleta2.join();
		atleta3.join();
		atleta4.join();
		atleta5.join();
		atleta6.join();
		atleta7.join();
		atleta8.join();

		System.out.println(" ");
		System.out.println(""
				+ "Fin de la carrera");
		System.out.println("---------------------------");
		carrera.mostrarResultado();
	}

}
