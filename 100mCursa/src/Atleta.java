
public class Atleta extends Thread {

	private int dorsal = 0;
	private Carrera carrera = new Carrera();
	private float velocidad;
	
	public Atleta(int dorsal, Carrera carrera) {
		super();
		this.dorsal = dorsal;
		this.setName("corredor" + dorsal);
		this.velocidad = (long) ((Math.random() * ((11000 - 9000) + 1)) + 9000);
		this.carrera = carrera;
	}


	public void run () {
		try {
			this.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			while(this.carrera.iniciarCarrera() == false ) {
				this.wait();
			}
			System.out.println("El corredor" + this.getName() + "empieza a correr ");
			this.sleep(1000);
			
			this.carrera.getCorredores().add(this);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public float getVelocidad() {
		return velocidad;
	}


	public void setVelocidad(float velocidad) {
		this.velocidad = velocidad;
	}


	public int getDorsal() {
		return dorsal;
	}


	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}


	public Carrera getCarrera() {
		return carrera;
	}


	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}


	@Override
	public String toString() {
		return "Atleta [dorsal=" + dorsal +  ", velocidad=" + velocidad + "]";
	}
	
	
	
}
