
public class Main  {

	public static void main(String[] args) throws InterruptedException {
		Tic tic = new Tic("Tic");
		Thread t1 = new Thread(tic); 
		Tac tac = new Tac("Tac");
		Thread t2 = new Thread(tac); 
		
		t1.start();
		Thread.sleep(1000);
		t2.start();
	}
}
