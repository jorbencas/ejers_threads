import java.util.Scanner;

import ChatDatagram.Interlucotor;

public class main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce mensaje de A");
        String introducido = teclado.nextLine();
        System.out.println("Introduce mensaje de B");
        String introducido2 = teclado.nextLine();

        while (!introducido.equalsIgnoreCase("adeu") && !introducido2.equalsIgnoreCase("adeu")) {
            Interlucotor inter = new Interlucotor("A", introducido);
            Interlucotor inter2 = new Interlucotor("B", introducido2);

            inter.start();
            inter2.start();
            
            inter.join();
            inter2.join();
      
            
            System.out.println("Introduce mensaje de A");
            introducido = teclado.nextLine();
            System.out.println("Introduce mensaje de B");
            introducido2 = teclado.nextLine();
        }
	}

}
