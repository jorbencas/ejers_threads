import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main(String[] args) throws IOException {
		int  puerto = 3001;
		 ServerSocket server_socket;
		 Socket socket;
		 DataInputStream entrada;
		 DataOutputStream salida;
		 String mensa = "11";
		 try {
			 System.out.println("El servidor esta escuchando...");
				server_socket = new ServerSocket(puerto);
				socket = server_socket.accept();
				salida = new DataOutputStream(socket.getOutputStream());
				entrada = new DataInputStream(socket.getInputStream());

				do{
					mensa = entrada.readUTF();
					String mensage = "Hola Cliente " + mensa;
					salida.writeUTF(mensage);
				}while(!mensa.equalsIgnoreCase("3"));
				System.out.println("Todos los mensagues han sido enviados.");
				entrada.close();
				salida.close();
				socket.close();
				server_socket.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
